// A crude hack to re-introduce the CS/LTH navigation menu for mobile devices.
// NOTE: this is rather fragile wrt. changes in the site. Will likely require
// changes at some point when our Typo3 templates change.
//
// Patrik Persson, 2016

<div id="extraNavigation"></div>

<script type="text/javascript">
  (function() {
    var menuCreated = false;
    
    var checkMenu = function() {
      var navigation = document.getElementById("content_navigation");
      var menu = document.getElementById("extraNavigation");
      
      if (navigation.offsetWidth <= 0 || navigation.offsetHeight <= 0) { // sidebar menu invisible?
        
        // clear menu (if already populated)
        while (menu.firstChild) {
          menu.removeChild(menu.firstChild);
        }
        
        menu.style.display = "block";

        // An UL contains a number of LIs, each containing an A link.
        // We can't copy the whole thing, since some LIs contain sublists
        // (and we don't want those). It's a bit of a mess.
        
        for (var i = 0; i < navigation.children.length; i++) {
          var ul = navigation.children[i];
          if (ul.tagName === "UL") {
            var items = ul.children; // many li

            for (var j = 0; j < items.length; j++) {
              var li = items[j];
              if (li.tagName === "LI") {
                var itemContents = li.children;
                for (var k = 0; k < itemContents.length; k++) {
                  var a = itemContents[k];
                  if (a.tagName === "A") {
                    var link = a.href;
                    if (link != window.top.location) {
                      var element = document.createElement("div");
                      var newA = a.cloneNode(true);
                      newA.className = "extraNavigationLink";
                      element.className = "extraNavigationItem";
                      element.appendChild(newA);
                      menu.appendChild(element);
                    }
                  }
                }
              }
            }
          }
        }
        menuCreated = true;
      } else {
        menu.style.display = "none";
      }
    };
    
    window.addEventListener("load", checkMenu);
    window.addEventListener("resize", checkMenu);
  })();
</script>

<style type="text/css">
  #extraNavigation {
    padding: 10px;
    background-color: #f8f3ea;
    display: none;
  }
  div.extraNavigationItem {
    background-color: #9c6114;
    padding: 10px 20px 10px 20px;
    margin: 5px;
  }
  a.extraNavigationLink {
    color: #fff;
  }
  a.extraNavigationLink:link {
    text-decoration: none;
  }
  a.extraNavigationLink:visited {
    text-decoration: none;
  }
  a.extraNavigationLink:hover {
    text-decoration: underline;
  }
  a.extraNavigationLink:active {
    text-decoration: underline;
  }
</style>
